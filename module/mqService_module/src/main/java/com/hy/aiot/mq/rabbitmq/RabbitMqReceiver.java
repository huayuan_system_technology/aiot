package com.hy.aiot.mq.rabbitmq;


import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @since : 消息消费者
 *
 */

@Component
public class RabbitMqReceiver {

    @RabbitListener
    public void process(String message){
        System.out.println("Message is:" + message);
    }
}
