package com.hy.aiot.mq;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @since : 消息服务，提供异步处理AIO能力
 * @author :Jgm
 * @Date:2022.04.04
 */

@SpringBootApplication(exclude = {org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
@EnableDiscoveryClient
@EnableSwagger2
@EnableRabbit
@EnableHystrix
@EnableHystrixDashboard
public class mqApplication {
    public static void main(String[] args){ SpringApplication.run(mqApplication.class, args);}
}
