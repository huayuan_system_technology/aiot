package com.hy.aiot.cloudadmin;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.turbine.EnableTurbine;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Since: 服务监控
 * 1. 显示应用信息
 * 2. 显示在线状态
 * 3. 日志级别管理
 * 4. JMX beans 管理
 * 5. 会话和线程管理
 * 6. 应用请求管理
 * 7. 应用运行参数信息
 * 8. 显示熔断器信息
 * @author :Jgm
 * @Date:2022.04.09
 *
 *
 */



@SpringBootApplication(exclude = {org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
@EnableDiscoveryClient
@EnableSwagger2
@EnableHystrix
@EnableHystrixDashboard
@EnableAdminServer
public class cloudAdminApplication {
    public static void main(String[] args){ SpringApplication.run(cloudAdminApplication.class, args);}
}
