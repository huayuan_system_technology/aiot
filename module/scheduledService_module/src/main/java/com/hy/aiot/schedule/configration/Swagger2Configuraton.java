package com.hy.aiot.schedule.configration;

import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import org.xmlpull.v1.XmlPullParserException;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
public class Swagger2Configuraton {

    @Bean
    public Docket createRestApi() throws IOException,XmlPullParserException{

        try {
            MavenXpp3Reader reader = new MavenXpp3Reader();
            Model model = reader.read(new FileReader("pom.xml"));
            ApiInfo apiInfo = new ApiInfoBuilder()
                    .title("华远基础架构---定时任务服务API")
                    .description("定时任务服务api")
                    .version(model.getVersion())
                    .build();

            List<ResponseMessage> responseMessage = new ArrayList();
            responseMessage.add(new ResponseMessageBuilder().code(404).message("找不到资源").build());
            responseMessage.add(new ResponseMessageBuilder().code(500).message("服务器内部错误").build());

            return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo)
                    .globalResponseMessage(RequestMethod.GET, responseMessage)
                    .globalResponseMessage(RequestMethod.POST, responseMessage)
                    .globalResponseMessage(RequestMethod.PUT, responseMessage)
                    .globalResponseMessage(RequestMethod.DELETE, responseMessage)
                    .select()
                    .apis(RequestHandlerSelectors.basePackage(""))
                    .paths(PathSelectors.any())
                    .build();
        }catch (Exception e){
            e.printStackTrace();
        }
                  return null;

    }
}
