package com.hy.aiot.schedule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @since :定时任务
 * @author :Jgm
 * @Date:2022.04.04
 *
 */

@SpringBootApplication(exclude = {org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
@EnableDiscoveryClient
@EnableSwagger2
@EnableHystrix
@EnableHystrixDashboard
public class scheduledServiceApplication {
    public static void main(String[] args){ SpringApplication.run(scheduledServiceApplication.class, args);}
}
