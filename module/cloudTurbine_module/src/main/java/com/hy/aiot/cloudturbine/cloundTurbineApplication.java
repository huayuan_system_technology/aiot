package com.hy.aiot.cloudturbine;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.turbine.EnableTurbine;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Since： Turbine 负责所有的应用的hystrix.stream数据，为了分担压力所以将 Turbine独立的一个微服务
 */

@SpringBootApplication(exclude = {org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
@EnableDiscoveryClient
@EnableSwagger2
@EnableHystrix
@EnableHystrixDashboard
@EnableTurbine
public class cloundTurbineApplication {
    public static void main(String[] args){ SpringApplication.run(cloundTurbineApplication.class, args);}
}
