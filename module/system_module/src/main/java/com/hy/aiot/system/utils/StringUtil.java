package com.hy.aiot.system.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.Map;

public class StringUtil extends StringUtils {
    public static <T> boolean isEmpty(T[] obj) {
        return (obj == null) || (obj.length == 0);
    }

    public static boolean isEmpty(Object obj) {
        if (obj == null) {
            return true;
        }
        if ((obj instanceof String)) {
            return (String.valueOf(obj)).trim().isEmpty();
        }
        if (obj instanceof CharSequence) {
            return ((CharSequence) obj).length() == 0;
        }
        if (obj instanceof Number) {
            return false;
        }
        return false;
    }
    public static String formateStr (String str){
        if (isEmpty(str)){
            return "";
        } else {
            return str;
        }
    }
    @SuppressWarnings("rawtypes")
    public static boolean isEmpty(Collection obj) {
        return (obj == null) || (obj.isEmpty());
    }

    @SuppressWarnings("rawtypes")
    public static boolean isEmpty(Map obj) {
        return (obj == null) || (obj.isEmpty());
    }

}
