package com.hy.aiot.system;

import com.hy.aiot.auth.intf.test.FeignInterface;
import com.hy.aiot.system.redis.RedisClientImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.unit.DataSize;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.MultipartConfigElement;

/**
 * @Since:系统管理微服务
 * @Author：Jgm
 * @Date:2020.07.25
 */

@SpringBootApplication(exclude = {org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
@EnableDiscoveryClient
//扫描接口包，让Feign客户端接口注入
@EnableFeignClients(basePackages = {"com.hy.aiot.auth.intf","com.hy.aiot.system.intf"})
@EnableSwagger2
//开启AOP代理自动配置
@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableTransactionManagement
@EnableHystrix
@EnableHystrixDashboard
public class SystemApplication {

    public static void main(String[] args){ SpringApplication.run(SystemApplication.class, args);}
    //配置上传文件大小
    @Bean
    public MultipartConfigElement multipartConfigElement(){
        MultipartConfigFactory factory = new MultipartConfigFactory();
        //单个数据大小
        factory.setMaxFileSize(DataSize.ofBytes(1000 * 1024L * 1024L));
        //总上传文件数据大小
        factory.setMaxRequestSize(DataSize.ofBytes(1000 * 1024L * 1024L));
        return factory.createMultipartConfig();
    }
}
