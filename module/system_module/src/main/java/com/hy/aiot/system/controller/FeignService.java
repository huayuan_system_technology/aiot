package com.hy.aiot.system.controller;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "hello-service",fallback = FeignFallBack.class)
public interface FeignService {

    @RequestMapping("/hello")
    String hello();
}


