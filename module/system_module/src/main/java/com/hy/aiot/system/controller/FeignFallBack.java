package com.hy.aiot.system.controller;

import org.springframework.stereotype.Component;

@Component
public class FeignFallBack implements FeignService {

    @Override
    public String hello(){
        return "error";
    }
}
