package com.hy.aiot.system.configration;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Component
@ConfigurationProperties(prefix = "jedis.pool")
@Configuration
public class JedisConfig {
    private static final long serialVersionUID = 1L;

    @org.springframework.beans.factory.annotation.Value("${spring.redis.host}")
    private String host;
    @Value("${spring.redis.port}")
    private Integer port;
    @Value("${spring.redis.password}")
    private String password;
    @Value("${spring.redis.database}")
    private Integer database;
    @Value("${spring.redis.connect-timeout}")
    private Integer timeout;
    @Value("${spring.redis.connect-maxTotal}")
    private Integer maxTotal;

    /**
     * 最大空闲数
     */
    @Value("${spring.redis.connect-maxIdle}")
    private Integer maxIdle;
    /**
     * 最小空闲数
     */
    @Value("${spring.redis.connect-minIdle}")
    private Integer minIdle;
    /**
     * borrow一个jedis实例时最大等待时间,毫秒
     */
    @Value("${spring.redis.connect-maxWaitMillis}")
    private Long maxWaitMillis;
    /**
     * 获取实例时检查可用性
     */
    @Value("${spring.redis.connect-testOnBorrow}")
    private boolean testOnBorrow;
    /**
     * 归还实例时检查可用性
     */
    @Value("${spring.redis.connect-testOnReturn}")
    private boolean testOnReturn;
    /** dle object evitor两次扫描之间要sleep的毫秒数*/
//    private Integer timeBetweenEvictionRunsMillis;使用默认值

    /**
     * 如果为true，表示有一个idle object evitor线程对idle object进行扫描，如果validate失败，
     * 此object会被从pool中drop掉；这一项只有在timeBetweenEvictionRunsMillis大于0时才有意义
     */
//    private boolean testWhileIdle;使用默认值
    @Bean
    public JedisPool jedisPool() {
        JedisPool jedisPool =null;
        try {
            JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
            jedisPoolConfig.setMaxTotal(maxTotal);
            jedisPoolConfig.setMaxIdle(maxIdle);
            jedisPoolConfig.setMaxWaitMillis(maxWaitMillis);
            jedisPoolConfig.setTestOnBorrow(testOnBorrow);
            jedisPoolConfig.setTestOnReturn(testOnReturn);
//        jedisPoolConfig.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
//        jedisPoolConfig.setTestWhileIdle(testWhileIdle);
//            JedisPool jedisPool = new JedisPool(jedisPoolConfig, host, port, timeout, password, database);
            jedisPool = new JedisPool(jedisPoolConfig, host, port, timeout, password, database);
//            return jedisPool;
        }catch (Exception e){
           e.printStackTrace();
           System.out.println(jedisPool);
        }
        return jedisPool;
    }

    public Integer getDatabase() {
        return database == null ? 0 : database;
    }

    public JedisConfig setDatabase(Integer database) {
        this.database = database;
        return this;
    }

    public Integer getTimeout() {
        return timeout == null ? 3000 : timeout;
    }

    public JedisConfig setTimeout(Integer timeout) {
        this.timeout = timeout;
        return this;
    }

    public String getHost() {
        return host == null ? "127.0.0.1" : host;
    }

    public JedisConfig setHost(String host) {
        this.host = host;
        return this;
    }

    public Integer getPort() {
        return port == null ? 6379 : port;
    }

    public JedisConfig setPort(Integer port) {
        this.port = port;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public JedisConfig setPassword(String password) {
        this.password = password;
        return this;
    }

    public Integer getMaxTotal() {
        return maxTotal;
    }

    public JedisConfig setMaxTotal(Integer maxTotal) {
        this.maxTotal = maxTotal;
        return this;
    }

    public Integer getMaxIdle() {
        return maxIdle;
    }

    public JedisConfig setMaxIdle(Integer maxIdle) {
        this.maxIdle = maxIdle;
        return this;
    }

    public Integer getMinIdle() {
        return minIdle;
    }

    public JedisConfig setMinIdle(Integer minIdle) {
        this.minIdle = minIdle;
        return this;
    }

    public Long getMaxWaitMillis() {
        return maxWaitMillis;
    }

    public JedisConfig setMaxWaitMillis(Long maxWaitMillis) {
        this.maxWaitMillis = maxWaitMillis;
        return this;
    }

    public boolean isTestOnBorrow() {
        return testOnBorrow;
    }

//    public Integer getTimeBetweenEvictionRunsMillis() {
//        return timeBetweenEvictionRunsMillis == null ? 1800000 : timeBetweenEvictionRunsMillis;
//    }
//
//    public JedisConfig setTimeBetweenEvictionRunsMillis(Integer timeBetweenEvictionRunsMillis) {
//        this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
//        return this;
//    }

    public JedisConfig setTestOnBorrow(boolean testOnBorrow) {
        this.testOnBorrow = testOnBorrow;
        return this;
    }

    public boolean isTestOnReturn() {
        return testOnReturn;
    }

    public JedisConfig setTestOnReturn(boolean testOnReturn) {
        this.testOnReturn = testOnReturn;
        return this;
    }

//    public boolean isTestWhileIdle() {
//        return testWhileIdle;
//    }
//
//    public JedisConfig setTestWhileIdle(boolean testWhileIdle) {
//        this.testWhileIdle = testWhileIdle;
//        return this;
//    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
