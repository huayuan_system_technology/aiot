package com.hy.aiot.system.redis;

import redis.clients.jedis.Jedis;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * @Description Jedis客户端接口
 * @Author Jgm
 * @Date 2021-08-31
 * @Version 1.0
 */

public interface RedisClient {
    /**
     * 执行function中的逻辑
     * @param function
     * @param <T>
     * @return
     */
    <T> T execute(Function<Jedis, T> function);

    String set(final String key, final Object value);

    /**
     * set值,同时设置过期时间,单位秒
     * @param key
     * @param value
     * @param time
     * @return
     */
    String set(final String key, final Object value, final int time);

    /**
     * 完全自主的set方法
     * @param key
     * @param value
     * @param nxxx nxxx NX|XX, NX -- Only set the key if it does not already exist. XX -- Only set the key
     * @param expx expx EX|PX, expire time units: EX = seconds; PX = milliseconds
     * @param time time expire time in the units of {@param #expx}
     * @return
     */
    String set(final String key, final Object value, final String nxxx, final String expx,
               final int time);

    /**
     * 获取String型value
     * @param key
     * @return
     */
    String get(String key);

    /**
     * 获取<T>型value
     * @param key
     * @param clazz
     * @param <T>
     * @return
     */
    <T> T get(String key, Class<T> clazz);

    /**
     * 获取元素类型为<T>的List类型value
     * @param key
     * @param clazz
     * @param <T>
     * @return
     */
    <T> List<T> getList(String key, Class<T> clazz);

    /**
     * 设置过期时间,单位毫秒
     * @param key
     * @param milliseconds
     * @return
     */
    Long pexpire(final String key, final long milliseconds);

    /**
     * 设置指定单位的过期时间,最小到毫秒
     * @param key
     * @param times
     * @param timeUnit
     * @return
     */
    Long pexpire(final String key, final long times, final TimeUnit timeUnit);

    /**
     * 设置过期时间,单位秒
     * @param key
     * @param seconds
     * @return
     */
    Long expire(final String key, final int seconds);

    /**
     * 是否存在key
     * @param key
     * @return
     */
    Boolean exists(final String key);

    /**
     * 设置hash型数据
     * @param key
     * @param field
     * @param value
     * @return
     */
    Long hset(final String key, final String field, final Object value);

    /**
     * 获取hash中的String类型的value
     * @param key
     * @param field
     * @return
     */
    String hget(final String key, final String field);

    /**
     * 获取hash中T型的value
     * @param key
     * @param field
     * @param clazz
     * @param <T>
     * @return
     */
    <T> T hget(final String key, final String field, Class<T> clazz);

    /**
     * hash中指定字段自增指定值
     * @param key
     * @param field
     * @param value
     * @return
     */
    Long hincrBy(final String key, final String field, final long value);

    /**
     * 获取hash中元素为T型的List型value
     * @param key
     * @param field
     * @param clazz
     * @param <T>
     * @return
     */
    <T> List<T> hgetList(final String key, final String field, Class<T> clazz);

    /**
     * 删除key
     * @param keys
     */
    Long del(String... keys);

    /**
     * 添加元素进set
     * @param key
     * @param members
     * @return
     */
    Long sadd(final String key, final Object... members);

    /**
     * 从set中弹出一个元素
     * @param key
     * @return
     */
    String spop(final String key);

    /**
     * 从set中弹出一个元素并转成目标类型
     * @param key
     * @return
     */
    <T> T spop(final String key, Class<T> clazz);

    /**
     * 从set中弹出一批元素
     * @param key
     * @param count
     * @return
     * @Deprecated 生产环境redis版本为2.8, 不接受count参数
     */
    @Deprecated
    Set<String> spop(final String key, final long count);

    /**
     * 从set中弹出一批元素
     * @param key
     * @param count
     * @return
     * @Deprecated 生产环境redis版本为2.8, 不接受count参数
     */
    @Deprecated
    <T> Set<T> spop(final String key, final long count, Class<T> clazz);
}
