package com.hy.aiot.system.entity;

public class Result {
    private String code;
    private String msg;
    private long count;
    private Object data;

    public Result() {}

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Result(String code, String msg, long count, Object data) {
        this.code = code;
        this.msg = msg;
        this.count = count;
        this.data = data;
    }

    @Override
    public String toString() {
        return "Result{"
                + "code='"
                + code
                + '\''
                + ", msg='"
                + msg
                + '\''
                + ", count="
                + count
                + ", data="
                + data
                + '}';
    }
}
