package com.hy.aiot.system.appClient;

import com.alibaba.fastjson.JSONObject;
import com.hy.aiot.system.entity.Result;
import com.hy.aiot.system.utils.CommonConstant;
import com.hy.aiot.system.utils.Util;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @since : App登录接口处理类
 * @author :Jgm
 * @Date:2022.05.20
 *
 */

@Controller
@RequestMapping("/appClient")
@Slf4j
public class AppClientController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * @api {POST} /exhibitCard 新增
     * @apiGroup exhibitCard
     * @apiVersion 0.0.1
     */
    @ResponseBody
    @RequestMapping(value = "/process", method = RequestMethod.POST)
    @ApiOperation("app业务接口")
    public Result process(
            @RequestParam String txcode,
            @RequestParam String AppKey,
            @RequestParam String Nonce,
            @RequestParam String CurTime,
            @RequestParam String CheckNum,
            String data
    ) {

        if (null == txcode || "".equals(txcode)) {
            logger.error(txcode + ":txcode is null------------");
            return new Result(CommonConstant.code.MISSING_PARAMETER_CODE, "txcode is null!", 0, null);
        }
        if (null == AppKey || "".equals(AppKey)) {
            logger.error(AppKey + ":AppKey is null------------");
            return new Result(CommonConstant.code.MISSING_PARAMETER_CODE, "AppKey is null!", 0, null);
        } else {
            if (!AppKey.equals(AppConstant.AppKey)) {
                return new Result(CommonConstant.code.MISSING_PARAMETER_CODE, "AppKey is error!", 0, null);
            }
        }
        if (null == Nonce || "".equals(Nonce)) {
            logger.error(Nonce + ":Nonce is null------------");
            return new Result(CommonConstant.code.MISSING_PARAMETER_CODE, "Nonce is null!", 0, null);
        } else {
            if (!Nonce.equals(AppConstant.Nonce)) {
                return new Result(CommonConstant.code.MISSING_PARAMETER_CODE, "Nonce is error!", 0, null);
            }
        }
        if (null == CurTime || "".equals(CurTime)) {
            logger.error(txcode + ":CurTime is null------------");
            return new Result(CommonConstant.code.MISSING_PARAMETER_CODE, "CurTime is null!", 0, null);
        }
        if (null == CheckNum || "".equals(CheckNum)) {
            logger.error(CheckNum + ":CheckNum is null------------");
            return new Result(CommonConstant.code.MISSING_PARAMETER_CODE, "CheckNum is null!", 0, null);
        } else {
            String checkNum = Util.SHA1(AppConstant.Nonce + AppConstant.AppSecret + CurTime);
            if (!CheckNum.equals(checkNum)) {
                return new Result(CommonConstant.code.MISSING_PARAMETER_CODE, "CheckNum is error!", 0, null);
            }
        }

        Result result = new Result();

        try {
            JSONObject json = JSONObject.parseObject(data);

              /*
      查询所有的展会分类
       */
//            if ("expoTypeAll".equals(txcode)) {
//                //展会一级分类接口
//                result = expoTypeAll(json);
//            } else if ("getAppinfo".equals(txcode)) {
//                //获取app信息
//                result = getAppinfo(json);
//            }  else if ("mailAndPhoneVerificationCode".equals(txcode)) {
//                //获取注册于登录一体接口的短信或者邮箱验证码
//                result = mailAndPhoneVerificationCode(json);
//            } else if ("registerAndLogin".equals(txcode)){
//
//            }


            //业务逻辑处理


        }catch (Exception e){
            e.printStackTrace();
            logger.error(txcode + ":业务操作异常-------------" + data);
            logger.error("Exception:-------------" + e.toString());
            return new Result(CommonConstant.code.OPERATIONAL_ANOMALIES_CODE, "业务操作异常！", 0, null);
        }
        return result;
    }
}
