package com.hy.aiot.system.controller;


import com.hy.aiot.auth.intf.test.FeignInterface;
import com.hy.aiot.system.intf.FeignInterface1;
import com.hy.aiot.system.netty.PushMsgService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
public class FeiginTestController {

    @Autowired
    FeignInterface1 feignInterface1;
    @Autowired
    FeignInterface feignInterface;

    @Autowired
    PushMsgService pushMsgService;


////    @GetMapping("/FeignTest")
//    @PostMapping("/FeignTest")
//   public String test(@RequestBody String username){
//   System.out.println("Feign 调用成功");
//   System.out.print(username);
//   feignInterface1.test(username);
//
//       return username;
//   }

//    @RequestMapping("/Test")
    @PostMapping("/Test")
//    SysUserVo SysUserLogin(JSONObject jsonObject);
    public String Test(@RequestBody String jsonObject){
          System.out.println("系統模塊---Feign 调用成功");
        return "系統模塊----Fegin調用成功";
    }

    @GetMapping("/webSocketTest")
    public String pushOne(@RequestParam String uid){
        System.out.println("测试WebSokcet---消息推送功能");
        pushMsgService.pushMsgToOne(uid,"hello");
        return "测试WebSokcet---消息推送功能";
    }

    //测试只有登录拿到token才能拿到数据
    @GetMapping("/get")
    public String aaa() {
        System.out.println("token验证通过了");
        return "token验证通过了";
    }
}
