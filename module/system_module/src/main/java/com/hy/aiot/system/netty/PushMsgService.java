package com.hy.aiot.system.netty;

/**
 * 推送消息接口及实现类
 */

public interface PushMsgService {
    void pushMsgToOne(String userId,String msg);
    void pushMsgToAll(String msg);
}
