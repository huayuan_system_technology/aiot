package com.hy.aiot.system.controller.clientController.wechatApplet;

import com.hy.aiot.system.redis.RedisClientImpl;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @since :微信小程序业务模块
 * 1. 上传图片
 */

@RestController
@Slf4j
public class wechatAppletController {

    //FastDfS参数配置声明跟踪器客户端对象
    TrackerClient trackerClient = null;
    // 声明存储器客户端对象
    StorageClient storageClient = null;
    // 声明跟踪器服务对象
    TrackerServer trackerServer = null;
    // 声明存储器服务对象
    StorageServer storageServer = null;
    StorageClient1 storageClient1 = null;
    private static Properties FDFS_PROP = new Properties();

    private static final Logger logger = LoggerFactory.getLogger(wechatAppletController.class);

    //1.上传图片
    @RequestMapping(value = "/UploadWebFile", method = RequestMethod.POST)
    @ResponseBody
    public Object UploadWebFile(@RequestParam("file") MultipartFile file, HttpServletRequest request
    ) throws Exception {
        String result[] = null;
        List list = new ArrayList();
        if (!file.isEmpty() ) {
            try {
                System.out.println("=========start uploade file===============");
                long startTime = System.currentTimeMillis();
                String curPath = this.getClass().getResource("").getPath();
                System.out.println("Upload file [" + file.getOriginalFilename() + "] startTime::" + startTime + ",current path:" + curPath);

                InputStream fdfsInputStream = getClass().getClassLoader().getResourceAsStream("fdfs_client.properties");
                //String filePath = new ClassPathResource("application.properties").getFile().getAbsolutePath();
                //System.out.println("current full path:" + curPath + "file path:" + filePath + "，filePath:" + filePath);
                FDFS_PROP.load(fdfsInputStream);
                fdfsInputStream.close();

                ClientGlobal.initByProperties(FDFS_PROP);
                TrackerClient trackerClient = new TrackerClient();
                TrackerServer trackerServer = trackerClient.getConnection();
                StorageClient storageClient = new StorageClient(trackerServer, storageServer);
                //MultipartHttpServletRequest接收文件，多张图片上传功能
                MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
                List<MultipartFile> FilesList = multipartHttpServletRequest.getFiles("file");//获取上传的文件
                for (int i = 0; i < FilesList.size(); i++) {
                    MultipartFile multipartFile = FilesList.get(i);
                    String name = multipartFile.getName();
                    String uploadFileName = multipartFile.getOriginalFilename();
                    String fileExtName = "";
                    if (uploadFileName.contains(".")) {
                        fileExtName = uploadFileName.substring(uploadFileName.lastIndexOf(".") + 1);
                    }
                    NameValuePair[] metaList = new NameValuePair[2];
                    metaList[0] = new NameValuePair("fileName", uploadFileName);
                    metaList[1] = new NameValuePair("fileExtName", fileExtName);
                    result = storageClient.upload_file(null, multipartFile.getBytes(), fileExtName, metaList);
                    System.out.println("Uploade_file[" + uploadFileName + "] ok");
                    System.out.println(result[0]);//文件存储所在组，如：group1,group2等；
                    System.out.println(result[1]); //文件在服务器上的路径及文件名
                    list.add(result[0]);
                    list.add(result[1]);
                }
                long endTime = System.currentTimeMillis();
                System.out.println("=========Successful file upload===============");
                System.out.println("Upload file[" + file.getOriginalFilename() + "] endTime::" + endTime);
            } catch (Exception e) {
                logger.info("文件为空或者设备不合法",e);
                e.printStackTrace();
                System.out.println("文件为空或者设备不合法");
            }
        }
        JSONArray json = JSONArray.fromObject(list);
        if (0 != json.size()) {
            return json;
        }
        return "UploadFile are empty or equipment is illegal!!!";
    }
}
