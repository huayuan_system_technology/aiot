package com.hy.aiot.system.task;

import com.hy.aiot.system.redis.RedisClientImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.support.SimpleTriggerContext;

/**
 * @Since:定时任务
 */
@Configuration
@EnableScheduling
public class SaticScheduleTask {
    @Autowired
    RedisClientImpl redisClient;
    @Scheduled(fixedRate = 5000)
    private void configureTasks(){
        String a = "11111111111111";
        redisClient.set("jgm",a);
        String b = redisClient.get("jgm");
        System.out.println("从Redis获取的值为:"+b);
    }
}
