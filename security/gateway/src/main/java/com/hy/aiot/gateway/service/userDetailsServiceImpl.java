package com.hy.aiot.gateway.service;

import com.hy.aiot.gateway.dao.userMapper;
import com.hy.aiot.gateway.entity.user;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 验证身份:加载响应的UserDetails，看看是否和用户输入的账号、密码、权限等信息匹配;
 */

@Service
public class userDetailsServiceImpl implements UserDetailsService {
    @Resource
    private IUserService IUserService;
    @Resource
    userMapper userMapper;

    //重写loadUserByUsername方法：根据用户名获取用户，用户角色，权限等信息。
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
        UserDetails userDetails = null;
        user user = new user();
        try {
            if (user!=null){
                user=IUserService.getUserByname(username);
                Collection<GrantedAuthority> authList = getAuthorities(username);
                if (authList!=null){
                userDetails = new User(username,user.getPassword().toLowerCase(),true,true,true,true,authList);
                }
//                userDetails = new User(username,user.getPassword().toLowerCase(),true,true,true,true,authList);
            }
//            user=IUserService.getUserByname(username);
//            Collection<GrantedAuthority> authList = getAuthorities(username);
//            userDetails = new User(username,user.getPassword().toLowerCase(),true,true,true,true,authList);
            }catch (Exception e){
            e.printStackTrace();
        }
        return userDetails;
    }

    //获取用户的角色权限---临时设定的一个角色
    private Collection<GrantedAuthority> getAuthorities(String username){
        List<GrantedAuthority> authList = null;
        try{
            user user = new user();
            user=IUserService.getUserByname(username);
            if (user!=null){
                String ROLE_guest = user.getRole();
                System.out.println("依据username获取用户角色名:"+ROLE_guest);
                authList = new ArrayList<GrantedAuthority>();
//            authList.add(new SimpleGrantedAuthority("ROLE_guest"));
                authList.add(new SimpleGrantedAuthority(ROLE_guest));
//            return authList;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return authList;
    }

}
