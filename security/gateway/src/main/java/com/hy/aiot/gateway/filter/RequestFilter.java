package com.hy.aiot.gateway.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class RequestFilter extends ZuulFilter {

    @Override
    public boolean shouldFilter(){
        return true;
    }

    @Override
    public Object run(){
        RequestContext context = RequestContext.getCurrentContext();
        HttpServletRequest request = context.getRequest();
        String URL = request.getRequestURI();
        if (URL.contains("http://localhost:8889")){
            context.setSendZuulResponse(true);
        }
//        context.setSendZuulResponse(true);
        return null;
    }

    @Override
    public String filterType(){
        return "pre";
    }

    @Override
    public int filterOrder(){
        return 0;
    }
}
