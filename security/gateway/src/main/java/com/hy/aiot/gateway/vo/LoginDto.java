package com.hy.aiot.gateway.vo;

import lombok.Data;

/**
 * 登录Vo
 *
 */

@Data
public class LoginDto {
    String username;
    String password;
    String role;

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }
}
