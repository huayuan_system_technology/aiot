package com.hy.aiot.gateway.service;

import com.hy.aiot.gateway.dao.userMapper;
import com.hy.aiot.gateway.entity.user;
import com.hy.aiot.gateway.utils.desUtils;
import com.hy.aiot.gateway.utils.jwtTokenUtil;
import com.hy.aiot.gateway.utils.md5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import javax.servlet.http.HttpServletRequest;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private userMapper userDao;
    @Autowired
//    private UserDetailsService userDetailsService;
    private userDetailsServiceImpl userDetailsService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private jwtTokenUtil jwtTokenUtil;
    @Value("${jwt.tokenHead}")
    private String tokenHead;
    static Charset charset = Charset.forName("UTF-8");

    @Override
    public user getUserByname(String username){
        user user = new user();
        try {
            user=userDao.getUserByname(username);
        }catch (Exception e){
            e.printStackTrace();
        }
        return user;
    }

//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
//
//        user user = getUserByname(username);
//
//        if (user==null){
//            throw new UsernameNotFoundException("Invalid User");
//        }
//        else {
//            Set<GrantedAuthority> grantedAuthorities = user.getRole()
//                    .stream().map().c
//        }
//    }


    @Override
    public int insertSelective(user record) {
        return userDao.insertSelective(record);
    }

//    @Override
//    public JwtUser getUserByName(String userName) {
//        user user = userDao.getUserByName(userName);
//        JwtUser jwtUser = new JwtUser(user);
//        return jwtUser;
//    }

    //Login登录----
    @Override
    public Map login(String username, String password, HttpServletRequest request) {
        Map<String, Object> tokenMap = new HashMap<>();
        try {
            //登录----关键性的登录的方法loadUserByUsername
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            if (null == userDetails || passwordEncoder.matches(password, userDetails.getPassword())) {
                tokenMap.put("code", 400);
                tokenMap.put("message", "账号或密码错误或者为空");
                System.out.println("账号或密码错误或者为空");
                return tokenMap;
            }
            System.out.println("---------数据库取出密码值---------------"+userDetails.getPassword());
            String md5Str = md5Utils.strToMD5(password);
            System.out.println("---------传值password通过md5加密值------"+md5Str);
            //数据库密码与传来的密码校验，如果密码相同，校验通过
            if (md5Str.equals(userDetails.getPassword())){
                //更新security登录用户对象
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails
                        , null, userDetails.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                SecurityContextHolder.getContext().getAuthentication();
                System.out.println("获取Authentication值为:"+new SecurityContextHolder());
                User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                String Username = user.getUsername();
                //生成token
                String token = jwtTokenUtil.generateToken(userDetails);
                tokenMap.put("token", token);
                tokenMap.put("tokenHead", tokenHead);
                tokenMap.put("code", 200);
                tokenMap.put("message", "登录成功");
                System.out.println("返回的token值为:"+tokenMap.toString());
            }
            else {
                tokenMap.put("code",400);
                tokenMap.put("message","登录失败，密码错误");
                System.out.println("登录失败，密码错误");
                return tokenMap;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return tokenMap;
    }


    //第三方平台接入登录---QQ/微信/Facebook/推特账号登录
    @Override
    public Map externallogin(String username,String password,HttpServletRequest request){
        Map<String, Object> tokenMap = new HashMap<>();
        SecretKey secretKey;
        //登录----关键性的登录的方法loadUserByUsername
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        try {
            if (username==null){
                tokenMap.put("code", 400);
                tokenMap.put("message", "第三方平台输入参数为空");
                System.out.println("---第三方平台输入参数为空----");
                return tokenMap;
            }
            System.out.println("---------数据库取出密码值---------------"+userDetails.getPassword());
            String md5Str = md5Utils.strToMD5(password);
            System.out.println("---------传值password通过md5加密值------"+md5Str);
            //数据库密码与传来的密码校验，如果密码相同，校验通过
            if (md5Str.equals(userDetails.getPassword())){
                //更新security登录用户对象
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails
                        , null, userDetails.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                SecurityContextHolder.getContext().getAuthentication();
                System.out.println("获取Authentication值为:"+new SecurityContextHolder());
                User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                String Username = user.getUsername();
                //生成token
                String token = jwtTokenUtil.generateToken(userDetails);
//                //AES加密
//                long timeStartEncry = System.currentTimeMillis();
//                // 生成密钥
//                secretKey = aesUtil.generateKey();
//                byte[] encryptResult = aesUtil.encrypt(token, secretKey);
//                long timeEndEncry = System.currentTimeMillis();
//                String token1 = new String(encryptResult, charset);
//                System.out.println("-------------加密后的结果为--------------" + token1);
                //DES加密
                String token1 = desUtils.encrypt(token);
                System.out.println("-------------加密后的结果为--------------" + token1);
                tokenMap.put("token", token1);
                tokenMap.put("tokenHead", tokenHead);
                tokenMap.put("code", 200);
                tokenMap.put("message", "登录成功");
                System.out.println("返回的token值为:"+tokenMap.toString());
            }
            else {
                tokenMap.put("code",400);
                tokenMap.put("message","登录失败，密码错误");
                System.out.println("登录失败，密码错误");
                return tokenMap;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return tokenMap;
    }

}
