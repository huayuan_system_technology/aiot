package com.hy.aiot.gateway.entity;

import java.util.Date;

public class user {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.username
     *
     * @mbg.generated
     */
    private String username;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.password
     *
     * @mbg.generated
     */
    private String password;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.role
     *
     * @mbg.generated
     */
    private String role;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.backup1
     *
     * @mbg.generated
     */
    private String backup1;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.backup2
     *
     * @mbg.generated
     */
    private String backup2;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.backup3
     *
     * @mbg.generated
     */
    private String backup3;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.backup4
     *
     * @mbg.generated
     */
    private String backup4;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.backup5
     *
     * @mbg.generated
     */
    private String backup5;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.backup6
     *
     * @mbg.generated
     */
    private String backup6;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.backup7
     *
     * @mbg.generated
     */
    private String backup7;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.backup8
     *
     * @mbg.generated
     */
    private String backup8;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user
     *
     * @mbg.generated
     */
    public user(Integer id, String username, String password, String role, String backup1, String backup2, String backup3, String backup4, String backup5, String backup6, String backup7, String backup8) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
        this.backup1 = backup1;
        this.backup2 = backup2;
        this.backup3 = backup3;
        this.backup4 = backup4;
        this.backup5 = backup5;
        this.backup6 = backup6;
        this.backup7 = backup7;
        this.backup8 = backup8;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user
     *
     * @mbg.generated
     */
    public user() {
        super();
    }

    public user(String host, String user, String selectPriv, String insertPriv, String updatePriv, String deletePriv, String createPriv, String dropPriv, String reloadPriv, String shutdownPriv, String processPriv, String filePriv, String grantPriv, String referencesPriv, String indexPriv, String alterPriv, String showDbPriv, String superPriv, String createTmpTablePriv, String lockTablesPriv, String executePriv, String replSlavePriv, String replClientPriv, String createViewPriv, String showViewPriv, String createRoutinePriv, String alterRoutinePriv, String createUserPriv, String eventPriv, String triggerPriv, String createTablespacePriv, String sslType, Integer maxQuestions, Integer maxUpdates, Integer maxConnections, Integer maxUserConnections, String plugin, String passwordExpired, Date passwordLastChanged, Short passwordLifetime, String accountLocked, String createRolePriv, String dropRolePriv, Short passwordReuseHistory, Short passwordReuseTime, String passwordRequireCurrent) {
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.id
     *
     * @return the value of user.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.id
     *
     * @param id the value for user.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.username
     *
     * @return the value of user.username
     *
     * @mbg.generated
     */
    public String getUsername() {
        return username;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.username
     *
     * @param username the value for user.username
     *
     * @mbg.generated
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.password
     *
     * @return the value of user.password
     *
     * @mbg.generated
     */
    public String getPassword() {
        return password;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.password
     *
     * @param password the value for user.password
     *
     * @mbg.generated
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.role
     *
     * @return the value of user.role
     *
     * @mbg.generated
     */
    public String getRole() {
        return role;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.role
     *
     * @param role the value for user.role
     *
     * @mbg.generated
     */
    public void setRole(String role) {
        this.role = role == null ? null : role.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.backup1
     *
     * @return the value of user.backup1
     *
     * @mbg.generated
     */
    public String getBackup1() {
        return backup1;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.backup1
     *
     * @param backup1 the value for user.backup1
     *
     * @mbg.generated
     */
    public void setBackup1(String backup1) {
        this.backup1 = backup1 == null ? null : backup1.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.backup2
     *
     * @return the value of user.backup2
     *
     * @mbg.generated
     */
    public String getBackup2() {
        return backup2;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.backup2
     *
     * @param backup2 the value for user.backup2
     *
     * @mbg.generated
     */
    public void setBackup2(String backup2) {
        this.backup2 = backup2 == null ? null : backup2.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.backup3
     *
     * @return the value of user.backup3
     *
     * @mbg.generated
     */
    public String getBackup3() {
        return backup3;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.backup3
     *
     * @param backup3 the value for user.backup3
     *
     * @mbg.generated
     */
    public void setBackup3(String backup3) {
        this.backup3 = backup3 == null ? null : backup3.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.backup4
     *
     * @return the value of user.backup4
     *
     * @mbg.generated
     */
    public String getBackup4() {
        return backup4;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.backup4
     *
     * @param backup4 the value for user.backup4
     *
     * @mbg.generated
     */
    public void setBackup4(String backup4) {
        this.backup4 = backup4 == null ? null : backup4.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.backup5
     *
     * @return the value of user.backup5
     *
     * @mbg.generated
     */
    public String getBackup5() {
        return backup5;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.backup5
     *
     * @param backup5 the value for user.backup5
     *
     * @mbg.generated
     */
    public void setBackup5(String backup5) {
        this.backup5 = backup5 == null ? null : backup5.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.backup6
     *
     * @return the value of user.backup6
     *
     * @mbg.generated
     */
    public String getBackup6() {
        return backup6;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.backup6
     *
     * @param backup6 the value for user.backup6
     *
     * @mbg.generated
     */
    public void setBackup6(String backup6) {
        this.backup6 = backup6 == null ? null : backup6.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.backup7
     *
     * @return the value of user.backup7
     *
     * @mbg.generated
     */
    public String getBackup7() {
        return backup7;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.backup7
     *
     * @param backup7 the value for user.backup7
     *
     * @mbg.generated
     */
    public void setBackup7(String backup7) {
        this.backup7 = backup7 == null ? null : backup7.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.backup8
     *
     * @return the value of user.backup8
     *
     * @mbg.generated
     */
    public String getBackup8() {
        return backup8;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.backup8
     *
     * @param backup8 the value for user.backup8
     *
     * @mbg.generated
     */
    public void setBackup8(String backup8) {
        this.backup8 = backup8 == null ? null : backup8.trim();
    }
}