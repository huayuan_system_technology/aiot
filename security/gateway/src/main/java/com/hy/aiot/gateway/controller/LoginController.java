package com.hy.aiot.gateway.controller;

import com.hy.aiot.gateway.entity.user;
import com.hy.aiot.gateway.service.IUserService;
import com.hy.aiot.gateway.utils.md5Utils;
import com.hy.aiot.gateway.vo.CommonResponse;
import com.hy.aiot.gateway.vo.LoginDto;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Jwt登录APi,Jwt可以实现APP/小程序与web公用一个服务端;因为APP不支持Session;
 *
 */

@RestController
@Slf4j
public class LoginController {
    @Autowired
    private IUserService userService;
    private static Logger logger = LoggerFactory.getLogger(LoginController.class);

    @PostMapping("/register")
    @ApiOperation("用户注册")
    public CommonResponse registerUser(@RequestBody Map<String, String> registerUser) {
        HashMap map = new HashMap();
        try{
            //1.判断是否已注册
            if ( userService.getUserByname(registerUser.get("username"))!=null) {
                System.out.println("此账号已注册，注册失败！");
                return new CommonResponse(1,"此账号已注册，注册失败！","");
            }
            else {
                //未注册，则进行注册
                user user = new user();
                int id = Integer.valueOf(UUID.randomUUID().toString().hashCode());
                //id可能是负数，如果为负数则需要转正数
                if (id<0){
                    id=-id;
                }
                System.out.println("---------用户id----------"+id);
                user.setId(id);
                user.setUsername(registerUser.get("username"));
                user.setPassword(md5Utils.strToMD5(registerUser.get("password")));
                user.setRole(registerUser.get("role"));
                userService.insertSelective(user);
                map.put("id",id);
                map.put("username",registerUser.get("username"));
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error("device login error", e);
          System.out.println("注册失败！");
        }
        System.out.println("用户注册成功！");
        return new CommonResponse(0,"注册成功",map);
    }

    // userlogin是用户登录的URL，login是第三方用户登录auth2.0的url,区分开来
    @PostMapping("/userlogin")
    @ApiOperation("用户登录")
    public Map login(@RequestBody LoginDto adminLoginParam, HttpServletRequest request) {
        return userService.login(adminLoginParam.getUsername(), adminLoginParam.getPassword(), request);
    }

    /**
     * @param adminLoginParam : 第三方授权码登录模式：支撑微信/QQ/推特/facebook授权登录模式
     * @param request
     * @return
     */

    @PostMapping("/externallogin")
    @ApiOperation("用户登录")
    public Map externallogin(@RequestBody LoginDto adminLoginParam, HttpServletRequest request) {
        return userService.externallogin(adminLoginParam.getUsername(), adminLoginParam.getPassword(),request);
    }


    //测试只有登录拿到token才能拿到数据
    @GetMapping("/get")
    public String aaa() {
        System.out.println("token验证通过了");
        return "token验证通过了";
    }


//    //用户退出接口
//    @PostMapping("/logout1")
//    public ModelAndView logout1(@RequestBody LoginDto adminLoginParam, HttpServletRequest request){
//        ModelAndView modelAndView = new ModelAndView();
//        try {
//            String username= adminLoginParam.getUsername();
//            System.out.println("username------值:----"+username);
////            String url = "http:localhost:8080/logout";
//            String url = "/logout";
//            //ModelAndView重定向
//            modelAndView.setViewName("redirect"+url);
//            System.out.println("用户退出成功！");
//            System.out.println("modelAndView----------"+modelAndView);
//
//        }catch (Exception e){
//          e.printStackTrace();
//        }
//        return modelAndView;
//    }

}
