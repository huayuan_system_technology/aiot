package com.hy.aiot.gateway.config;

import com.hy.aiot.gateway.filter.JWTAuthenticationEntryPoint;
import com.hy.aiot.gateway.filter.JWTAuthenticationFilter;
import com.hy.aiot.gateway.filter.JWTAuthorizationFilter;
import com.hy.aiot.gateway.service.UserLogoutHandler;
import com.hy.aiot.gateway.service.UserLogoutSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.LogoutFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private RestAuthorizationEntryPoint restAuthorizationEntryPoint;
    @Autowired
    private RestfulAccessDeniedHandler restfulAccessDeniedHandler;

    @Autowired
    private JWTAuthorizationFilter jwtAuthorizationFilter;

    //退出登录url，全局变量
    public String logoutUrl = "/logout";

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

//    添加AuthenticationProvider Bean
    @Bean
    public AuthenticationProvider daoAuthenticationProvider(){
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(userDetailsService());
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        daoAuthenticationProvider.setHideUserNotFoundExceptions(false);
        return daoAuthenticationProvider;
    }

    //密码模式配置
//    @Bean
//    public AuthenticationManager authenticationManager() throws Exception {
//        return super.authenticationManager();
//    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // @formatter: off
//        auth.inMemoryAuthentication()
//                .withUser("hellxz")
//                .password(passwordEncoder().encode("xyz"))
////                .password(passwordEncoder.encode("xyz"))
//                //密码模式
////                .authorities("ADMIN");
////                .authorities(new ArrayList<>(0));
//                //授权码模式
//                .authorities(Collections.emptyList());

        //解决security中异常UsernameNotFoundException
        auth.authenticationProvider(daoAuthenticationProvider());
        // @formatter: on
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        // SpringSercurity安全设置
//        http.csrf().disable();
        http
                .requestMatchers().antMatchers("/oauth/**","/login/**","/logout/**","/user/**")
                .and()
                .authorizeRequests()
                //配置swagger参数
                .antMatchers("v2/api-docs/**","/swagger-ui.html","/webjars/**").permitAll()
//                .antMatchers("/oauth/**").hasAnyRole().authenticated()
                .antMatchers("/oauth/**").hasAuthority("ROLE_ANONYMOUS")
                //SpringSecurity放行接口Url，用于接口调试
                .antMatchers("/FeignTest").permitAll()
                .antMatchers("/user/{username}").permitAll();
//                .and()
//                .formLogin().permitAll()
//                .and()
//                .cors().and()
//                .csrf().disable().authorizeRequests();
//                .and()

        http.cors().and()
                .csrf()
                .disable()
                .authorizeRequests().anyRequest().authenticated()
                .and()
                .formLogin()
//                .and().logout().addLogoutHandler(new UserLogoutHandler()).addLogoutHandler(new UserLogoutHandler())
                //增加退出操作
                .and().logout()
                //增加退出操作接口
                .logoutUrl("/logout")
                .permitAll()
                //退出逻辑执行成功后，重定向的请求接口
                .logoutSuccessUrl("/login.html")
                .permitAll()
                .addLogoutHandler(new UserLogoutHandler()).logoutSuccessHandler(new UserLogoutSuccessHandler(logoutUrl))
                //LogoutFilter拦截器之前校验token设置SecurityContextHolder上下文; Authentication
                .and()
//                .addFilter(new JWTAuthenticationFilter(authenticationManager()))
//                .addFilterBefore(new JWTAuthorizationFilter(authenticationManager()), LogoutFilter.class)
                .addFilterBefore(jwtAuthorizationFilter, LogoutFilter.class)
                //Jwt登出（退出）过滤器
                .addFilter(new JWTAuthorizationFilter(authenticationManager()))
                //获取用户登录的信息
                .addFilter(new JWTAuthenticationFilter(authenticationManager()))
                //不需要session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(new JWTAuthenticationEntryPoint());


//        http.logout().logoutUrl("/logout").logoutSuccessUrl("/login.html").permitAll();

        //   .and()
//                .authorizeRequests()
//                .antMatchers("/FeignTest").permitAll();新增login form支持用户及授权

        // 新增退出操作---------
//        http.csrf().disable()
//                .cors()
//                .and()
//                .authorizeRequests().anyRequest().authenticated()
//                .and().logout().addLogoutHandler(new UserLogoutHandler()).logoutSuccessHandler(new UserLogoutSuccessHandler())
//               //LogoutFilter拦截器之前校验token设置SecurityContextHolder上下文;
//                .and().addFilterBefore(new JWTAuthorizationFilter(authenticationManager()), LogoutFilter.class)
//                .addFilter(new JWTAuthorizationFilter(authenticationManager()))
//                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//                .and()
//                .exceptionHandling()
//                .authenticationEntryPoint(new JWTAuthenticationEntryPoint());


       //密码模式配置
//        http.antMatcher("/").authorizeRequests().antMatchers("/user/**").permitAll().anyRequest().authenticated()
//                //Basic登录
//                .and().httpBasic()
//                //关跨域保护
//                .and().csrf().disable();
        //关闭匿名用户
//        http.csrf().disable();
        //        http.authorizeRequests().antMatchers("/").permitAll()
//                .antMatchers("/user/**").hasRole("ROLE_ADMIN");
//                .antMatchers("").hasRole("");

//        http.authorizeRequests().antMatchers("/user/**").permitAll().antMatchers("/user/**")
//                .hasRole("ADMIN").anyRequest().authenticated().and().csrf().disable();


//        http
//                .headers()
//                .frameOptions().disable()
//                .httpStrictTransportSecurity().disable() //需要在iframe进行嵌套
//                // 由于使用的是JWT，我们这里不需要csrf
//                .and()
//                .csrf().disable()
//                .cors().and()
//                .httpBasic()
//                .and()
//                // 基于token，所以不需要session
//                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//                .and()
//                .authorizeRequests()
//                .antMatchers("/auth/**", "/message/send/**", "/", "/toLogin", "/toIndex", "/main", "/nav/**", "/csrf", "/static/**",
//                        "/login/**","/logout/**","/user/**").permitAll()
//                .antMatchers("/api/kepu/**").permitAll()
//                //测试接口，临时打开
//                .antMatchers("/FeignTest").permitAll()
////                .antMatchers("/wechat/wechatAppletAuthorizeLogin").permitAll()
//                .anyRequest().authenticated();
//        ;
//        // 禁用缓存
//        http.headers().cacheControl();
    }


}