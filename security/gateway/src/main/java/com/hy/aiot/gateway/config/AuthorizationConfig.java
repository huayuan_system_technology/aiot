//package com.hy.aiot.gateway.config;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
//import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
//import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
//import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
//
///**
// * @since :auth2.o认证服务器配置类
// * @author :Jgm
// * @Date:2022.07.7
// *
// */
//
//@Configuration
//@EnableAuthorizationServer
//public class AuthorizationConfig extends AuthorizationServerConfigurerAdapter {
//
//    //密码模式配置
////    @Autowired
////    private AuthenticationManager authenticationManager;
//
//    @Qualifier("userServiceImpl")
//    @Autowired
//    private UserDetailsService userDetailsService;
//
//    @Autowired
//    private PasswordEncoder passwordEncoder;
//
//    @Override
//    public void configure(ClientDetailsServiceConfigurer clients)throws Exception{
//        //密码模式配置
//        //@formatter:off
////        clients.inMemory()
////                .withClient("client-a")
////                .secret(passwordEncoder.encode("client-a-secret"))
////                .authorizedGrantTypes("password")
////                .scopes("read_scope");
//        //@formatter:on
//
//        //授权码模式配置
//        // @formatter: off
//        clients.inMemory()
//                .withClient("client-a") //客户端唯一标识（client_id）
//                .secret(passwordEncoder.encode("client-a-secret")) //客户端的密码(client_secret)，这里的密码应该是加密后的
//                .authorizedGrantTypes("authorization_code") //授权模式标识
//                .scopes("read_user_info") //作用域
//                .resourceIds("resource1") //资源id
//                .redirectUris("http://localhost:9001/callback"); //回调地址
//        // @formatter: on
//    }
//
////    @Override
////    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
////        endpoints.authenticationManager(authenticationManager);
////    }
//
//    @Override
//    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception{
//        //设置userDetailsService刷新token时候会用到
//        endpoints.userDetailsService(userDetailsService);
//    }
//
//
//    @Override
//    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
//        //允许表单提交
//        security.allowFormAuthenticationForClients()
//                .checkTokenAccess("isAuthenticated()");
//    }
//
//}
