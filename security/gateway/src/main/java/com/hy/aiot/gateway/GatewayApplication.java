package com.hy.aiot.gateway;

import com.hy.aiot.gateway.filter.RequestFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.context.SecurityContextHolder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Since:网关服务器
 * @Author:Jgm
 * @Date:2020.04.01
 */

@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
@EnableSwagger2
@EnableHystrix
@EnableHystrixDashboard
public class GatewayApplication {
        public static void main(String[] args){ SpringApplication.run(GatewayApplication.class, args);}

    @Bean
        public RequestFilter logFilter(){
                return new RequestFilter();
    }

}
