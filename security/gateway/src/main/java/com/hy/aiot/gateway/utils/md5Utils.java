package com.hy.aiot.gateway.utils;

import org.springframework.util.DigestUtils;

import java.io.UnsupportedEncodingException;

/**
 * @program: test
 * @description: MD5加密工具
 * @author: qiu bo yang
 * @create: 2020-10-13 22:04
 **/

public class md5Utils {

    /**
     * 字符串MD5加密
     *
     * @param str 字符串
     * @return MD5加密字符串
     */
    public static String strToMD5(String str) {
        String md5 = "";
        try {
            md5 = DigestUtils.md5DigestAsHex(str.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return md5;
    }

}
