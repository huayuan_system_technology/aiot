package com.hy.aiot.gateway.filter;

import com.hy.aiot.gateway.utils.desUtils;
import com.hy.aiot.gateway.utils.jwtTokenUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.crypto.SecretKey;
import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

/*
    @Since: Jwt登出（退出）过滤器
    @Date:2022.03.01
 */

@Component
public class JWTAuthorizationFilter extends OncePerRequestFilter {

   private final Logger logger = LoggerFactory.getLogger(JWTAuthorizationFilter.class);

    @Autowired
    private com.hy.aiot.gateway.service.userDetailsServiceImpl userDetailsService;

    public JWTAuthorizationFilter(AuthenticationManager authenticationManager) {
        super();
    }

    public JWTAuthorizationFilter(){}

        /**
         * 在过滤之前和之后执行的事件
         */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

        SecretKey secretKey;

        try {
            ServletContext context = request.getServletContext();
            ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
            jwtTokenUtil jwtTokenUtil = ctx.getBean(jwtTokenUtil.class);
            String tokenHeader = request.getHeader(jwtTokenUtil.TOKEN_HEADER);
            //判断来自第三方平台还是自有前端的特定字符  External:第三方平台，1,第三方平台;0,自有平台
            String tokenHeader1 = request.getHeader(jwtTokenUtil.External_HEADER);
            // 若请求头中没有Authorization信息 或是Authorization不以Bearer开头 则直接放行
            if (tokenHeader == null || !tokenHeader.startsWith(jwtTokenUtil.TOKEN_PREFIX))
            {
                chain.doFilter(request, response);
                return;
            }
            if (tokenHeader!=null&&tokenHeader.startsWith(jwtTokenUtil.TOKEN_PREFIX)){
                //External:第三方平台，1,第三方平台;0,自有平台
                if (tokenHeader1.equals("0")){
                    System.out.println("------开始自有平台登录JWT认证流程-------");
                    //token认证，依据token获取username
                    String authToken = tokenHeader.substring(jwtTokenUtil.TOKEN_PREFIX.length(),tokenHeader.length());
                    System.out.println("------------tokenHeader---------------"+tokenHeader);
                    System.out.println("------------authToken---------------"+authToken);
                    String username = jwtTokenUtil.getUserNameFromToken(authToken);
                    logger.info("checking username",username);
                    if (username!=null&&SecurityContextHolder.getContext().getAuthentication()==null){
                        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
                        System.out.println("-------------userDetails---------------"+userDetails);
                        if (jwtTokenUtil.validateToken(authToken,userDetails)==true){
                            //更新security登录用户对象 Principal
                            UsernamePasswordAuthenticationToken authentication =
                                    new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
                            System.out.println("----------------userDetails.getAuthorities()--------------------"+userDetails.getAuthorities());
                            authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                            logger.info("authenticated user:{}",username);
                            //建立安全上下文
                            SecurityContextHolder.getContext().setAuthentication(authentication);
                            System.out.println("----------结束自有平台Jwt认证/JWT认证成功！-------------");
                        }
                    }
                }
                if (tokenHeader1.equals("1")){
                    System.out.println("------进入第三方平台登录JWT认证流程-------");
                    //token认证，依据token获取username
                    String authToken1 = tokenHeader.substring(jwtTokenUtil.TOKEN_PREFIX.length(),tokenHeader.length());
                    System.out.println("------------tokenHeader1---------------"+tokenHeader1);
                    System.out.println("------------authToken1---------------"+authToken1);
                    String authToken = desUtils.decrypt(authToken1);
                    System.out.println("------------解密后authToken值---------------"+authToken);
                    String username = jwtTokenUtil.getUserNameFromToken(authToken);
                    logger.info("checking username",username);
                    if (username!=null&&SecurityContextHolder.getContext().getAuthentication()==null){
                        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
                        System.out.println("-------------userDetails---------------"+userDetails);
                        if (jwtTokenUtil.validateToken(authToken,userDetails)==true){
                            //更新security登录用户对象 Principal
                            UsernamePasswordAuthenticationToken authentication =
                                    new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
                            System.out.println("----------------userDetails.getAuthorities()--------------------"+userDetails.getAuthorities());
                            authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                            logger.info("authenticated user:{}",username);
                            //建立安全上下文
                            SecurityContextHolder.getContext().setAuthentication(authentication);
                            System.out.println("----------第三方平台登录Jwt认证成功！-------------");
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        chain.doFilter(request,response);
    }

    /**
     * 从token中获取用户信息并新建一个token
     *
     * @param tokenHeader 字符串形式的Token请求头
     * @return 带用户名和密码以及权限的Authentication
     */
    private UsernamePasswordAuthenticationToken getAuthentication(String tokenHeader) {
        // 去掉前缀 获取Token字符串
        String token = tokenHeader.replace(jwtTokenUtil.TOKEN_PREFIX, "");
        // 从Token中解密获取用户名
        String username = jwtTokenUtil.getUsername(token);
        // 从Token中解密获取用户角色
        String role = jwtTokenUtil.getUserRole(token);
        // 将[ROLE_XXX,ROLE_YYY]格式的角色字符串转换为数组
        String[] roles = StringUtils.strip(role, "[]").split(", ");
        Collection<SimpleGrantedAuthority> authorities=new ArrayList<>();
        for (String s:roles)
        {
            authorities.add(new SimpleGrantedAuthority(s));
        }
        if (username != null)
        {
            return new UsernamePasswordAuthenticationToken(username, null,authorities);
        }
        return null;
    }
}
