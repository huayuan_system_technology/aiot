package com.hy.aiot.gateway.service;

import com.hy.aiot.gateway.entity.user;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

//public interface IUserService extends UserDetailsService {
    public interface IUserService  {
    int insertSelective(user record);
//    JwtUser getUserByName(String userName);
    user getUserByname(String username);
    Map login(String username, String password, HttpServletRequest request);
//    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
    //第三方平台接入登录
    Map externallogin(String username,String password,HttpServletRequest request);
}
