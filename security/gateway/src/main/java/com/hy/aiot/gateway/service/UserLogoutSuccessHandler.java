package com.hy.aiot.gateway.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hy.aiot.gateway.vo.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @Since:
 * 如果实现了自定义的 LogoutSuccessHandler 就不必要设置LogoutConfigurer#logoutSuccessUrl(String logoutSuccessUrl)
 * 该处理器处理后会响应给前端。可以转发到其它控制器，重定向到登录页面，也可以自行实现其它 MediaType ,可以是 json 或者页面
 * @Date:2022.02.28
 * @author :Jgm
 *
 */

@Slf4j
public class UserLogoutSuccessHandler implements LogoutSuccessHandler {
    private static Logger log = LoggerFactory.getLogger(UserLogoutSuccessHandler.class);

    /**
     *  退出登录url,可以在yml里面指定
     */

    public String logoutUrl = "/logout";
    public ObjectMapper objectMapper = new ObjectMapper();
    public UserLogoutSuccessHandler(String logoutUrl){
        this.logoutUrl=logoutUrl;
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
       log.info("退出成功！");
        try {
//           user user = (user) authentication.getPrincipal();
//           String username = user.getUsername();
//           log.info("username: {}  is offline now", username);
//        responseJsonWriter(response, new CommonResponse<>().ok("退出成功"));
            //如果没有指定退出成功的页面，equalsIgnoreCase不区分大小写
            if (StringUtils.equalsIgnoreCase("/logout",logoutUrl)){
                response.setContentType("application/json;charset=UTF-8");
                response.getWriter().write(objectMapper.writeValueAsString(new CommonResponse(1,"退出成功",null)));
            }else {
                //重定向到退出成功登录页面
                response.sendRedirect(logoutUrl);
            }
           System.out.println("---------退出成功!!-------------");
       }catch (Exception e){
           e.printStackTrace();
       }
    }

    private static void responseJsonWriter(HttpServletResponse response, CommonResponse rest) throws IOException {
        try {
            response.setStatus(HttpServletResponse.SC_OK);
            response.setCharacterEncoding("utf-8");
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            ObjectMapper objectMapper = new ObjectMapper();
            String resBody = objectMapper.writeValueAsString(rest);
            PrintWriter printWriter = response.getWriter();
            printWriter.print(resBody);
            printWriter.flush();
            printWriter.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
