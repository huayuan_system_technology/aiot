package com.hy.aiot.gateway.utils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.security.SecureRandom;

/**
 * DES加密工具类
 */

public class desUtils {

    private static final String PASSWORD = "9588028820109132570743325311898426347857298773549468758875018579537757772163084478873699447306034466200616411960574122434059469100235892702736860872901247123456";

    public desUtils() {
    }

    /**
     * 加密
     * @param datasource
     * @return
     */
    public static String encrypt(String datasource) {
        try{
            DESKeySpec desKey = new DESKeySpec(PASSWORD.getBytes());
            //创建一个密匙工厂，获取secretKey
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey secretKey = keyFactory.generateSecret(desKey);
            //指定获取DES的Cipher对象
            Cipher cipher = Cipher.getInstance("DES");
            //用密匙初始化Cipher对象
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, new SecureRandom());
            //数据加密
            return parseByte2HexStr(cipher.doFinal(datasource.getBytes("utf-8")));
        }catch(Throwable e){
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 解密
     * @param src
     * @return
     */
    public static String decrypt(String src){
        try{
            // 创建一个DESKeySpec对象，PASSWORD可任意指定
            DESKeySpec desKey = new DESKeySpec(PASSWORD.getBytes());
            // 创建一个密匙工厂
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            // 生成密钥
            SecretKey secretkey = keyFactory.generateSecret(desKey);
            // 指定获取DES的Cipher对象
            Cipher cipher = Cipher.getInstance("DES");
            // 用密匙初始化Cipher对象
            cipher.init(Cipher.DECRYPT_MODE, secretkey, new SecureRandom());
            // 真正开始解密操作
            return new String(cipher.doFinal(parseHexStr2Byte(src)));
        }catch(Throwable e){
            e.printStackTrace();
        }
        return null;
    }

    /**将二进制转换成16进制
     * @param buf
     * @return
     */
    public static String parseByte2HexStr(byte buf[]) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < buf.length; i++) {
            String hex = Integer.toHexString(buf[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            sb.append(hex.toUpperCase());
        }
        return sb.toString();
    }

    /**将16进制转换为二进制
     * @param hexStr
     * @return
     */
    public static byte[] parseHexStr2Byte(String hexStr) {
        if (hexStr.length() < 1) {
            return null;
        }
        byte[] result = new byte[hexStr.length()/2];
        for (int i = 0;i< hexStr.length()/2; i++) {
            int high = Integer.parseInt(hexStr.substring(i*2, i*2+1), 16);
            int low = Integer.parseInt(hexStr.substring(i*2+1, i*2+2), 16);
            result[i] = (byte) (high * 16 + low);
        }
        return result;
    }

    public static void main(String[] args) {
        String str = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyb290IiwiY3JlYXRlZCI6MTY2ODU2NDQxNzY5MiwiZXhwIjoxNjY5MTY5MjE3fQ.xHdZtb3PWVjG0TRMhputP40IyfYFh2VKxFeeRUHpgvYV7esTx5S769PaJk9s8DW0N1X5s0r42IsCIZ2BN8l7xA";

        String result = encrypt(str);
        System.out.println("encode result is ："+result);
        //直接将如上内容解密
        try {
            String decryResult = decrypt(result);
            System.out.println("Decode result is ："+decryResult);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }


}
