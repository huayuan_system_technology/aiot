package com.hy.aiot.gateway.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class CommonResponse<T> implements Serializable {

    //请求成功返回码为：0000
    private static final int successCode = 0;

    @ApiModelProperty("返回数据")


    private T data;

    @ApiModelProperty("返回码状态，小于0表示出错")
    private Integer code;

    @ApiModelProperty("返回错误描述")
    private String message;

    public CommonResponse() {
        this.code = successCode;
        this.message = "请求成功";
    }

    public CommonResponse(Integer code, String msg) {
        this();
        this.code = code;
        this.message = msg;
    }

    public CommonResponse(Integer code, String msg, T data) {
        this();
        this.code = code;
        this.message = msg;
        this.data = data;
    }

    public CommonResponse(T data) {
        this();
        this.data = data;
    }
}
