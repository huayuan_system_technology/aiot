package com.hy.aiot.gateway.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hy.aiot.gateway.vo.CommonResponse;
//import jdk.internal.org.objectweb.asm.util.Printer;
import lombok.extern.slf4j.Slf4j;
import org.apache.coyote.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.trace.http.HttpTrace;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @since :自定义 LogoutHandler
 * 1. 默认情况下清除认证信息 （invalidateHttpSession）和 Session 失效（invalidateHttpSession） 已经由内置的SecurityContextLogoutHandler 来完成
 *   自定义的 LogoutHandler 会在SecurityContextLogoutHandler 来执行;
 * 2. 实现 LogoutHandler,可以从 logout 方法的 authentication 变量中获取当前用户信息。
 *   可以通过这个来实现具体想要的业务。比如记录用户下线退出时间、IP 等等。
 * @Date:2022.02.28
 * @author :Jgm
 *
 */

@Slf4j
public class UserLogoutHandler implements LogoutHandler {
    private static Logger log = LoggerFactory.getLogger(UserLogoutHandler.class);
    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        try {
//            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//            String username = user.getUsername();
//            log.info("username: {}  is offline now", username);
            System.out.println("--------退出成功-----------");
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
