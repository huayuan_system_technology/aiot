package com.hy.aiot.auth.controller;

import com.alibaba.fastjson.JSONObject;
import com.hy.aiot.auth.intf.authService.LoginService;
import com.hy.aiot.po.auth.vo.SysUserVo;
import com.hy.aiot.system.intf.FeginTestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
//import service.FeginTestService;

/**
 * @Since:服務提供者（本身也是接口），Feign客戶端接口實現類
 * @author :Jgm
 * @Date:2020.08.19
 */
@RestController
@Slf4j
//@RequestMapping(value = "v1/Login")
//public class LoginServiceImpl implements LoginService {
    public class LoginServiceImpl {
//    @Autowired
//    private LoginService loginService;
//    @Autowired
    private FeginTestService feginTestService;

//    @Override
//    @RequestMapping("/UserLogin")
//    @GetMapping("/UserLogin")
    @PostMapping("/UserLogin")
    public SysUserVo SysUserLogin(@RequestBody String jsonObject){
        feginTestService.Test(jsonObject);
        SysUserVo sysUserVo = JSONObject.parseObject(jsonObject,SysUserVo.class);
        System.out.println("權限模塊調用系統模塊---- Fegin服務提供者---成功");
        return sysUserVo;
    }

//    @Override
//    @RequestMapping("/GetUser")
    @GetMapping("/GetUser")
    public SysUserVo getUser(@RequestBody String jsonObject){

        System.out.println("獲取用戶信息 ---- Fegin服務提供者");
        return null;
    }
}
