//package com.hy.aiot.auth.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.core.annotation.Order;
//import org.springframework.http.HttpMethod;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.http.SessionCreationPolicy;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
//import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
//import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
//import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
//
//@Configuration
//@EnableResourceServer
//public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
//    @Bean
//    @Primary
//    public RemoteTokenServices remoteTokenServices() {
//        final RemoteTokenServices tokenServices = new RemoteTokenServices();
//        tokenServices.setClientId("client-a");
//        tokenServices.setClientSecret("client-a-secret");
////        tokenServices.setCheckTokenEndpointUrl("http://localhost:8080/oauth/check_token");
//        tokenServices.setCheckTokenEndpointUrl("http://localhost:8889/oauth/check_token");
//        return tokenServices;
//    }
//
//    @Override
//    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
//        //密码模式
////        resources.stateless(true);
//        //授权码模式
//        resources.resourceId("resource1").stateless(true);
//    }
//
//    @Override
//    public void configure(HttpSecurity http) throws Exception {
//        //session创建策略
//        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);
//        //所有请求需要认证
//        //授权码模式配置
////        http.authorizeRequests().anyRequest().authenticated();
//        //密码模式配置
////          http.authorizeRequests().anyRequest().permitAll();
//
//        //授权码模式配置，接口放行
//        http.headers().frameOptions().disable()
//                .and()
//                //Auth2.0放行接口Url，用户测试
//                .authorizeRequests()
//                //生成JSON格式的API文档访问地址:/v2/api-docs开放，不受Oauth2.0权限管理
//                .antMatchers(HttpMethod.GET,"/v2/api-docs").permitAll()
//                .antMatchers("/FeignTest").permitAll()
//                .antMatchers("/user/{username}").permitAll()
//                .antMatchers("/auth/**").permitAll()
//                .antMatchers("/user/get").permitAll()
////                .antMatchers("/user/uid/").permitAll()
//                .anyRequest().authenticated()
//                .and()
//                .csrf().disable();
//
////        http.authorizeRequests()
////                .antMatchers("/auth/**").permitAll()
////                .antMatchers("/user/**").authenticated();
//
//    }
//}
