package com.hy.aiot.auth;

import com.hy.aiot.system.intf.FeginTestService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.openfeign.EnableFeignClients;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * @Since:安全认证服务---(1. 用户登录；2.用户/角色/权限；3.用户管理)
 * @Author:Jgm
 * @Date：2020.04.01
 */

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.hy.aiot.system.intf")
@EnableSwagger2
@EnableHystrix
@EnableHystrixDashboard
public class AuthApplication {
    public static void main(String[] args){ SpringApplication.run(AuthApplication.class, args);}
}
