package com.hy.aiot.auth.controller;

import com.hy.aiot.auth.intf.test.FeignInterface;
import com.hy.aiot.system.intf.FeginTestService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Since:Auth接口工程Feign接口的实现类
 * @author Jgm
 * @Date:2020.09.20
 *
 */

@RestController
@Slf4j
public class FeignInterfaceController implements FeignInterface {

    @Autowired
    FeginTestService feginTestService;

    @Override
    //URL路径与服务实现类路径是一致
    @RequestMapping("/FeignTest")
    public String test(@RequestBody String username){
        System.out.println("Feign 调用成功");
        System.out.print(username.toString());
//        feignInterface1.test(username);
        String result = feginTestService.Test(username);
//        return username;
        System.out.println(result);
        return result;
    }

    //测试只有登录拿到token才能拿到数据
    @GetMapping("/get")
    @ApiOperation("get测试接口")
    public String aaa() {
        System.out.println("token验证通过了");
        return "token验证通过了";
    }

}
