package com.hy.aiot.auth.controller;

//import com.hy.aiot.auth.intf.test.FeignInterface;
import com.hy.aiot.auth.intf.test.FeignInterface;
import com.hy.aiot.system.intf.FeginTestService;
import com.hy.aiot.system.intf.FeignInterface1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

/**
 * 测试Feign调用
 */
@RestController
public class ResourceController {
    private static final Logger log = LoggerFactory.getLogger(ResourceController.class);
    @Autowired
    FeignInterface1 feignInterface;
//    @Autowired
//    FeginTestService feginTestService;
    @Autowired
    FeignInterface feignInterface1;



    @GetMapping("/user/{username}")
//    public UserVO user(@PathVariable String username) {
        public String user(@PathVariable String username) {
        log.info("{}", SecurityContextHolder.getContext().getAuthentication());
//        return new UserVO(username, username + "@foxmail.com");
        String u = null;
        try {
//            u = feignInterface.test(username);
            u = feignInterface1.test(username);
//            feignInterface.test(JSONObject.parseObject(username));
            System.out.print("OK");
            System.out.print(u);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "jgm123456"+ u;
}

    //    @GetMapping("/FeignTest")
//    @PostMapping("/FeignTest")
//    public String test(@RequestBody String username){
//        System.out.println("Feign 调用成功");
//        System.out.print(username);
//        feignInterface1.test(username);
//
//        return username;
//    }
}
