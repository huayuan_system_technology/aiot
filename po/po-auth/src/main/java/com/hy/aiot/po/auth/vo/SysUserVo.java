package com.hy.aiot.po.auth.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName: TpSysUserVO
 * @Description: TODO
 * @autor: Jgm
 * @date: 2020-08-11 11:18:38
 * @version: V1.0
 */

@Data
public class SysUserVo implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "用户编码", position = 0)
    private String code;
    @ApiModelProperty(value = "性别 0男 1女 2未知", position = 1)
    private String sex;
    @ApiModelProperty(value = "用户密码", position = 2)
    private String idmPassword;
    @ApiModelProperty(value = "更新人员", position = 3)
    private String udpateUser;
    @ApiModelProperty(value = "座机", position = 4)
    private String telephone;
    @ApiModelProperty(value = "更新时间", position = 5)
    private String updateTime;
    @ApiModelProperty(value = "备注", position = 6)
    private String remark;
    @ApiModelProperty(value = "用户姓名", position = 7)
    private String userName;
    @ApiModelProperty(value = "部门名称", position = 8)
    private String depName;
    @ApiModelProperty(value = "统一身份uid", position = 9)
    private String uid;
    @ApiModelProperty(value = "IP电话", position = 10)
    private String ipTelephone;
    @ApiModelProperty(value = "手机号码", position = 11)
    private String phoneNumber;
    @ApiModelProperty(value = "部门全路径", position = 12)
    private String depFname;
    @ApiModelProperty(value = "HR Id", position = 13)
    private String hrId;
    @ApiModelProperty(value = "创建时间", position = 14)
    private String createTime;
    @ApiModelProperty(value = "用户名", position = 15)
    private String loginName;
    @ApiModelProperty(value = "部门编号", position = 16)
    private String depId;
    @ApiModelProperty(value = "创建人员", position = 17)
    private String createUser;
    @ApiModelProperty(value = "id", position = 18)
    private String id;
    @ApiModelProperty(value = "机构编号", position = 19)
    private String orgId;
    @ApiModelProperty(value = "机构名称", position = 20)
    private String orgName;
    @ApiModelProperty(value = "邮箱", position = 21)
    private String email;
    @ApiModelProperty(value = "用户状态", position = 22)
    private String status;
    @ApiModelProperty(value = "角色", position = 23)
    private String roleName;
    @ApiModelProperty(value = "角色编码", position = 24)
    private String roleCode;
    @ApiModelProperty(value = "删除标记", position = 25)
    private String delFlag;

    public void setCode (String code) {
        this.code = code;
    }
    public String getCode() {
        return this.code;
    }
    public void setSex (String sex) {
        this.sex = sex;
    }
    public String getSex() {
        return this.sex;
    }
    public void setIdmPassword (String idmPassword) {
        this.idmPassword = idmPassword;
    }
    public String getIdmPassword() {
        return this.idmPassword;
    }
    public void setUdpateUser (String udpateUser) {
        this.udpateUser = udpateUser;
    }
    public String getUdpateUser() {
        return this.udpateUser;
    }
    public void setTelephone (String telephone) {
        this.telephone = telephone;
    }
    public String getTelephone() {
        return this.telephone;
    }
    public void setUpdateTime (String updateTime) {
        this.updateTime = updateTime;
    }
    public String getUpdateTime() {
        return this.updateTime;
    }
    public void setRemark (String remark) {
        this.remark = remark;
    }
    public String getRemark() {
        return this.remark;
    }
    public void setUserName (String userName) {
        this.userName = userName;
    }
    public String getUserName() {
        return this.userName;
    }
    public void setDepName (String depName) {
        this.depName = depName;
    }
    public String getDepName() {
        return this.depName;
    }
    public void setUid (String uid) {
        this.uid = uid;
    }
    public String getUid() {
        return this.uid;
    }
    public void setIpTelephone (String ipTelephone) {
        this.ipTelephone = ipTelephone;
    }
    public String getIpTelephone() {
        return this.ipTelephone;
    }
    public void setPhoneNumber (String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public String getPhoneNumber() {
        return this.phoneNumber;
    }
    public void setDepFname (String depFname) {
        this.depFname = depFname;
    }
    public String getDepFname() {
        return this.depFname;
    }
    public void setHrId (String hrId) {
        this.hrId = hrId;
    }
    public String getHrId() {
        return this.hrId;
    }
    public void setCreateTime (String createTime) {
        this.createTime = createTime;
    }
    public String getCreateTime() {
        return this.createTime;
    }
    public void setLoginName (String loginName) {
        this.loginName = loginName;
    }
    public String getLoginName() {
        return this.loginName;
    }
    public void setDepId (String depId) {
        this.depId = depId;
    }
    public String getDepId() {
        return this.depId;
    }
    public void setCreateUser (String createUser) {
        this.createUser = createUser;
    }
    public String getCreateUser() {
        return this.createUser;
    }
    public void setId (String id) {
        this.id = id;
    }
    public String getId() {
        return this.id;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public void setEmail (String email) {
        this.email = email;
    }
    public String getEmail() {
        return this.email;
    }
    public void setStatus (String status) {
        this.status = status;
    }
    public String getStatus() {
        return this.status;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }
}
