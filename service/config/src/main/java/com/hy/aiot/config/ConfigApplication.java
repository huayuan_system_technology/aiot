package com.hy.aiot.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * @Since:配置中心
 * @Author:Jgm
 * @Date：2020.04.01
 *
 */

@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigServer
@EnableHystrix
@EnableHystrixDashboard
public class ConfigApplication {
    public static void main(String[] args){ SpringApplication.run(ConfigApplication.class, args);}
}
