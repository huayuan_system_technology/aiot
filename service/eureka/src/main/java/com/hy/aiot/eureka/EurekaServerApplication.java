package com.hy.aiot.eureka;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 *
 * @Since:服务注册中心
 * @Author：Jgm
 * @Date:2020.04.01
 *
 */

@EnableEurekaServer
@SpringBootApplication
@EnableHystrix
@EnableHystrixDashboard
public class EurekaServerApplication {
   public static void main(String[] args){new SpringApplicationBuilder(EurekaServerApplication.class).run(args);}
}
