package com.hy.aiot.eureka.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @Since:关闭Eureka账号密码登录，关闭Security，减少开发量
 * @Author:Jgm
 * @Date:2020.03.19
 */
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private static Logger logger = LoggerFactory.getLogger(SecurityConfiguration.class);

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //关闭网页认证
        http.httpBasic().disable();
        //Eureka 默认启用了csrf检验，可以将其disable调
        http.csrf().disable();
    }
}
