//package kafka;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
////import com.github.pagehelper.util.StringUtil;
////import com.yunxi.constants.RedisKey;
////import com.yunxi.dto.message.ThingsLoginDTO;
////import com.yunxi.model.device.DeviceRegister;
////import com.yunxi.mqtt.DownStreamActionEnum;
////import com.yunxi.mqtt.MqttGateway;
////import com.yunxi.service.alarm.AlarmService;
////import com.yunxi.service.device.DeviceRegisterService;
////import com.yunxi.util.UUIDUtil;
//import dtoMessage.ThingsLoginDTO;
//import org.apache.kafka.clients.consumer.ConsumerRecord;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.kafka.annotation.KafkaListener;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//import java.util.Date;
//import java.util.Objects;
//import java.util.Optional;
//import java.util.concurrent.TimeUnit;
//
///**
// * @Since:Kafka监听设备消息类
// */
//
////@Component
//public class KafkaReceiver {
//
//
//    private static Logger logger = LoggerFactory.getLogger(KafkaReceiver.class);
//
////    @Resource
////    private MqttGateway mqttGateway;
////
////    @Resource
////    private AlarmService alarmService;
////
////    @Resource
////    private DeviceRegisterService registerService;
////
////    @Resource
////    private StringRedisTemplate stringRedisTemplate;
////
////    @Resource
////    private DeviceRegisterService deviceRegisterService;
//
//
//    @KafkaListener(topics = {"topic2"})
//    public void listen(ConsumerRecord<?, ?> record) {
//        Optional<?> kafkaMessage = Optional.ofNullable(record.value());
//        if (kafkaMessage.isPresent()) {
//            Object message = kafkaMessage.get();
//
//            logger.info("record =" + record);
//            logger.info("message =" + message);
//        }
//    }
//
//    @KafkaListener(topics = {"allThingsEntry"})
//    public void listenDownStreamMessage(ConsumerRecord<?, ?> record) {
//        Optional<?>
//                kafkaMessage = Optional.ofNullable(record.value());
//        if (kafkaMessage.isPresent()) {
//            Object message = kafkaMessage.get();
//
//            logger.debug("thingsLogin device login record =" + record);
//            logger.debug("thingsLogin device login message =" + message);
//            ThingsLoginDTO messagePayload = JSON.parseObject(message.toString(), ThingsLoginDTO.class);
//            if (Objects.isNull(messagePayload)) {
//                logger.error("listenDownStreamMessage device passed message is null message:" + message);
//            }
//
//            /**
//             * 处理预警信息
//             */
////            if (DownStreamActionEnum.ALERT.getAction().equals(messagePayload.getAction())) {
////                alarmService.acceptMessage(message.toString());
////            }
//
//            /**
//             * 处理图片信息( 由平台主动发起指令，设备收到后再回复photo指令并携带图片地址 - 主要用户巡检 )
//             */
////            if (DownStreamActionEnum.PHOTO.getAction().equalsIgnoreCase(messagePayload.getAction())) {
////                alarmService.acceptPhoto(message.toString());
////            }
//
//
//            /**
//             * 处理用户点击的获取图片信息
//             */
////            if (DownStreamActionEnum.TAKE_PHOTO.getAction().equalsIgnoreCase(messagePayload.getAction())) {
////                alarmService.acceptTakePhoto(message.toString());
////            }
//            /**
//             * 处理用户点击的获取视频图片信息
//             */
////            if (DownStreamActionEnum.TAKE_VIDEO.getAction().equalsIgnoreCase(messagePayload.getAction())) {
////                alarmService.acceptTakeVideo(message.toString());
////            }
//
//            //设备入网（Login--登录系统）
////            if (DownStreamActionEnum.CONNECT.getAction().endsWith(messagePayload.getAction())) {
////                //设备入网
////                processDeviceLogin(messagePayload);
////            } else if (DownStreamActionEnum.ONLINE.getAction().endsWith(messagePayload.getAction())) {
////                //处理设备在线 消息
////                processDeviceOnline(messagePayload);
////            } else {
////                logger.error(" down stream message action don't math. message action: " + messagePayload.getAction());
////            }
////        }
//
//    }
//
//
//    /**
//     *
//     * 处理下行消息login，处理设备登录系统业务逻辑
//     *
//     * @param messagePayload
//     */
////    private void processDeviceLogin(ThingsLoginDTO messagePayload) {
////        boolean isValid = false;
////        Date expiredTime = null;
////        int activeRst = 0;
////
////        DeviceRegister registerInfo = registerService.retrieveDeviceRegister(messagePayload.getFrom());
////        if (!Objects.isNull(registerInfo)) {
////            isValid = true;
////            expiredTime = registerInfo.getExpiredTime();
////            //查询登录的设备mac是否存在
////            String device_mac = messagePayload.getFrom();
////            DeviceRegister deviceRegister = deviceRegisterService.retrieveDeviceRegisterByMacAddress(device_mac);
////            if (deviceRegister!=null){
////                //如果存在，执行激活设备上线操作,实现设备登录操作
////                activeRst = registerService.activeDevice(messagePayload.getFrom(), null);
////            }
////        }
////        JSONObject result = new JSONObject();
////        result.put("from", APPLICATION_NAME);
////        result.put("action", messagePayload.getAction());
////        result.put("uuid", UUIDUtil.getUUID32());
////
////
////        JSONObject returnObj = new JSONObject();
////        returnObj.put("result", isValid);
////        returnObj.put("time_limit", expiredTime);
////
////        result.put("return", returnObj);
////
////        //发送mqtt 消息
////        mqttGateway.sendToMqtt(messagePayload.getFrom() + "_ThingEntry", 1, result.toJSONString());
////    }
//
//
//    /**
//     * 处理设备在线 消息
//     * 在线设备会定期发送心跳包机制
//     * 后台会存入redis
//     *
//     * @param messagePayload
//     */
////    private void processDeviceOnline(ThingsLoginDTO messagePayload) {
////        if (StringUtil.isNotEmpty(messagePayload.getFrom())) {
////            logger.debug("get device alive message from device, message info:" + messagePayload.toString());
////            ValueOperations<String, String> valueOp = stringRedisTemplate.opsForValue();
////            valueOp.set(RedisKey.MACHINE_ONLINE_KEY + messagePayload.getFrom(), (new Date()).toString(), RedisKey.MACHINE_ONLINE_PERIOD, TimeUnit.MINUTES);
////        }
//
//
//    }
//}
