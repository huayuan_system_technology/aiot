//package kafka;
//
//
//import com.alibaba.fastjson.JSON;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
//@RestController
//@RequestMapping(value = "/kafka")
//public class KafkaTestController {
//
//    @Autowired
//    KafkaSender kafkaSender;
//
//    SimpleDateFormat full_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzz");
//
//    @ResponseBody
//    @GetMapping("/ping")
//    public String ping() {
//        kafkaSender.send();
//        return JSON.toJSONString("iot service success Time: " + full_format.format(new Date()));
//    }
//
//}
