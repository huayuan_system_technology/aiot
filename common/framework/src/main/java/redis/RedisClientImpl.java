//package redis;
//
//import com.alibaba.fastjson.JSON;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import redis.clients.jedis.Jedis;
//import redis.clients.jedis.JedisPool;
//
//import java.util.Arrays;
//import java.util.List;
//import java.util.Objects;
//import java.util.Set;
//import java.util.concurrent.TimeUnit;
//import java.util.function.Function;
//import java.util.stream.Collectors;
//
///**
// * @Description Jedis客户端实现类
// * @Author DingYou
// * @Date 2018-07-10 11:43
// * @Version 1.0
// */
//@Service
//public class RedisClientImpl implements RedisClient {
//    private final JedisPool jedisPool;
//
//    @Autowired
//    public RedisClientImpl(JedisPool jedisPool) {
//        this.jedisPool = jedisPool;
//    }
//
//    /**
//     * 执行function中的逻辑
//     * @param function
//     * @param <T>
//     * @return
//     */
//    @Override
//    public <T> T execute(Function<Jedis, T> function) {
//        Jedis jedis = null;
//        try{
//            jedis = jedisPool.getResource();
//            return function.apply(jedis);
//        }catch (Exception e){
//            throw e;
//        }finally {
//            if(jedis != null){
//                jedis.close();
//            }
//        }
//    }
//
//    @Override
//    public String set(final String key, final Object value){
//        return execute(jedis -> jedis.set(key, getStringValue(value)));
//    }
//
//    /**
//     * set值,同时设置过期时间,单位秒
//     * @param key
//     * @param value
//     * @param time
//     * @return
//     */
//    @Override
//    public String set(final String key, final Object value, final int time){
//        return execute(jedis -> jedis.setex(key, time, getStringValue(value)));
//    }
//
//    /**
//     * 完全自主的set方法
//     * @param key
//     * @param value
//     * @param nxxx nxxx NX|XX, NX -- Only set the key if it does not already exist. XX -- Only set the key
//     * @param expx expx EX|PX, expire time units: EX = seconds; PX = milliseconds
//     * @param time time expire time in the units of {@param #expx}
//     * @return
//     */
//    @Override
//    public String set(final String key, final Object value, final String nxxx, final String expx,
//                      final int time){
//        return execute(jedis -> jedis.set(key, getStringValue(value), nxxx, expx, time));
//    }
//
//    /**
//     * 获取String型value
//     * @param key
//     * @return
//     */
//    @Override
//    public String get(String key){
//        return execute(jedis -> jedis.get(key));
//    }
//    /**
//     * 获取<T>型value
//     * @param key
//     * @param clazz
//     * @param <T>
//     * @return
//     */
//    @Override
//    public <T> T get(String key, Class<T> clazz){
//        String stringValue = get(key);
//        if(stringValue == null){
//            return null;
//        }
//        return JSON.parseObject(stringValue, clazz);
//    }
//    /**
//     * 获取元素类型为<T>的List类型value
//     * @param key
//     * @param clazz
//     * @param <T>
//     * @return
//     */
//    @Override
//    public <T> List<T> getList(String key, Class<T> clazz){
//        String stringValue = get(key);
//        if(stringValue == null){
//            return null;
//        }
//        return JSON.parseArray(stringValue, clazz);
//    }
//    /**
//     * 设置过期时间,单位毫秒
//     * @param key
//     * @param milliseconds
//     * @return
//     */
//    @Override
//    public Long pexpire(final String key, final long milliseconds){
//        return execute(jedis -> jedis.pexpire(key, milliseconds));
//    }
//
//    /**
//     * 设置指定单位的过期时间,最小到毫秒
//     * @param key
//     * @param times
//     * @param timeUnit
//     * @return
//     */
//    @Override
//    public Long pexpire(final String key, final long times, final TimeUnit timeUnit){
//        return execute(jedis -> jedis.pexpire(key, timeUnit.toMillis(times)));
//    }
//    /**
//     * 设置过期时间,单位秒
//     * @param key
//     * @param seconds
//     * @return
//     */
//    @Override
//    public Long expire(final String key, final int seconds){
//        return execute(jedis -> jedis.expire(key, seconds));
//    }
//    /**
//     * 是否存在key
//     * @param key
//     * @return
//     */
//    @Override
//    public Boolean exists(final String key){
//        return execute(jedis -> jedis.exists(key));
//    }
//
//    /**
//     * 设置hash型数据
//     * @param key
//     * @param field
//     * @param value
//     * @return
//     */
//    @Override
//    public Long hset(final String key, final String field, final Object value){
//        return execute(jedis -> jedis.hset(key, field, getStringValue(value)));
//    }
//    /**
//     * 获取hash中的String类型的value
//     * @param key
//     * @param field
//     * @return
//     */
//    @Override
//    public String hget(final String key, final String field){
//        return execute(jedis -> jedis.hget(key, field));
//    }
//    /**
//     * 获取hash中T型的value
//     * @param key
//     * @param field
//     * @param clazz
//     * @param <T>
//     * @return
//     */
//    @Override
//    public <T> T hget(final String key, final String field, Class<T> clazz){
//        String stringValue = execute(jedis -> jedis.hget(key, field));
//        if(stringValue == null){
//            return null;
//        }
//        return JSON.parseObject(stringValue, clazz);
//    }
//
//    /**
//     * hash中指定字段自增指定值
//     *
//     * @param key
//     * @param field
//     * @param value
//     * @return
//     */
//    @Override
//    public Long hincrBy(String key, String field, long value) {
//        return execute(jedis -> jedis.hincrBy(key, field, value));
//    }
//
//    /**
//     * 获取hash中元素为T型的List型value
//     * @param key
//     * @param field
//     * @param clazz
//     * @param <T>
//     * @return
//     */
//    @Override
//    public <T> List<T> hgetList(final String key, final String field, Class<T> clazz){
//        String stringValue = execute(jedis -> jedis.hget(key, field));
//        if(stringValue == null){
//            return null;
//        }
//        return JSON.parseArray(stringValue, clazz);
//    }
//
//    /**
//     * 删除key
//     *
//     * @param keys
//     */
//    @Override
//    public Long del(String... keys) {
//        return execute(jedis -> jedis.del(keys));
//    }
//
//    /**
//     * 添加元素进set
//     *
//     * @param key
//     * @param members
//     * @return
//     */
//    @Override
//    public Long sadd(String key, Object... members) {
//        String[] strArr = Arrays.stream(members).map(this::getStringValue).toArray(String[]::new);
//        return execute(jedis -> jedis.sadd(key, strArr));
//    }
//
//    /**
//     * 从set中弹出一个元素
//     *
//     * @param key
//     * @return
//     */
//    @Override
//    public String spop(String key) {
//        return execute(jedis -> jedis.spop(key));
//    }
//
//    /**
//     * 从set中弹出一个元素并转成目标类型
//     *
//     * @param key
//     * @param clazz
//     * @return
//     */
//    @Override
//    public <T> T spop(String key, Class<T> clazz) {
//        String spop = spop(key);
//        return spop == null ? null : JSON.parseObject(spop, clazz);
//    }
//
//    /**
//     * 从set中弹出一批元素
//     *
//     * @param key
//     * @param count
//     * @return
//     */
//    @Override
//    public Set<String> spop(String key, long count) {
//        return execute(jedis -> jedis.spop(key, count));
//    }
//
//    /**
//     * 从set中弹出一批元素
//     *
//     * @param key
//     * @param count
//     * @param clazz
//     * @return
//     */
//    @Override
//    public <T> Set<T> spop(String key, long count, Class<T> clazz) {
//        Set<String> spop = spop(key, count);
//        if(Objects.isNull(spop)){
//            return null;
//        }
//        return spop.stream().map(s -> JSON.parseObject(s, clazz)).collect(Collectors.toSet());
//    }
//
//    /**
//     * 将Object转成json
//     * @param value
//     * @return
//     */
//    private String getStringValue(Object value){
//        if(value == null){
//            return null;
//        }
//        if(value instanceof String){
//            return (String) value;
//        }
//        return JSON.toJSONString(value);
//    }
//}
