package com.hy.aiot.auth.intf.authService;

import com.alibaba.fastjson.JSONObject;
import com.hy.aiot.po.auth.vo.SysUserVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author: Jgm
 * @description: 權限微服務，Feigin客戶端接口，對外提供服務，供其它微服務工程Feign調用
 * @date: 2020/08/11 11:02
 */

@FeignClient(value = "auth-service-jgm")
public interface LoginService {

    /**
     * 登录表单提交
     */
//    @RequestMapping("/UserLogin")
//    @GetMapping("/UserLogin")
    @PostMapping("/UserLogin")
    SysUserVo SysUserLogin(@RequestBody String jsonObject);

    /**
     * 根据用户名和密码查询对应的用户
     *
     * @param username 用户名
     * @param password 密码
     */
//    @RequestMapping("/GetUser")
      @GetMapping("/GetUser")
    SysUserVo getUser(@RequestBody String jsonObject);


}
