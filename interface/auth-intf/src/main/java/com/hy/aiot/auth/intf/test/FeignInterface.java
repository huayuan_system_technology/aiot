package com.hy.aiot.auth.intf.test;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

//@FeignClient(value = "system-module-jgm")
@FeignClient(value = "auth-service-jgm")
@Service
public interface FeignInterface {

    //URL路径与服务实现类路径是一致
    @RequestMapping("/FeignTest")
//    @GetMapping("/FeignTest")
    public String test(@RequestBody String username);

}
