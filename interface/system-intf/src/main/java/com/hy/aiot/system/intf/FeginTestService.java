package com.hy.aiot.system.intf;


import com.alibaba.fastjson.JSONObject;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author: Jgm
 * @description: 系統模塊微服務，Feigin客戶端接口，對外提供服務，供其它微服務工程Feign調用
 * @date: 2020/08/11 11:02
 */

@FeignClient(value = "system-module-jgm")
public interface FeginTestService {


    /**
     * 測試接口
     */
//    @RequestMapping("/Test")
    @PostMapping("/Test")
//    SysUserVo SysUserLogin(JSONObject jsonObject);
    public String Test(@RequestBody String jsonObject);

}
